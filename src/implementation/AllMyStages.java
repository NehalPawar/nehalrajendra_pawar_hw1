/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.CpuCore;
import baseclasses.InstructionBase;
import baseclasses.PipelineRegister;
import baseclasses.PipelineStageBase;
import implementation.AllMyLatches.DecodeToExecute;
import implementation.AllMyLatches.ExecuteToMemory;
import implementation.AllMyLatches.FetchToDecode;
import implementation.AllMyLatches.MemoryToWriteback;
import utilitytypes.EnumOpcode;
import utilitytypes.Operand;
import voidtypes.VoidLatch;

/**
 * The AllMyStages class merely collects together all of the pipeline stage 
 * classes into one place.  You are free to split them out into top-level
 * classes.
 * 
 * Each inner class here implements the logic for a pipeline stage.
 * 
 * It is recommended that the compute methods be idempotent.  This means
 * that if compute is called multiple times in a clock cycle, it should
 * compute the same output for the same input.
 * 
 * How might we make updating the program counter idempotent?
 * 
 * @author
 */
public class AllMyStages {
    /*** Fetch Stage ***/
    static class Fetch extends PipelineStageBase<VoidLatch,FetchToDecode> {
        public Fetch(CpuCore core, PipelineRegister input, PipelineRegister output) {
            super(core, input, output);
        }
        
        @Override
        public String getStatus() {
            // Generate a string that helps you debug.
            return null;
        }

        @Override
        public void compute(VoidLatch input, FetchToDecode output) {
            GlobalData globals = (GlobalData)core.getGlobalResources();
            int pc = globals.program_counter;
            // Fetch the instruction
            InstructionBase ins = globals.program.getInstructionAt(pc);
            if (ins.isNull()) return;

            // Do something idempotent to compute the next program counter.
            
            // Don't forget branches, which MUST be resolved in the Decode
            // stage.  You will make use of global resources to commmunicate
            // between stages.
            
            // Your code goes here...
           // System.out.println("Fetch  " +ins);
          /*  if (nextStageCanAcceptWork()) {
            	output.setInstruction(ins);   
            	 	
			}*/
         
            	output.setInstruction(ins); 
            
        }
        
        @Override
        public boolean stageWaitingOnResource() {
            // Hint:  You will need to implement this for when branches
            // are being resolved.
       /* if(output_reg.isSlaveStalled())
        		return true;
        	else*/
            return false;
        }
        
        
        /**
         * This function is to advance state to the next clock cycle and
         * can be applied to any data that must be updated but which is
         * not stored in a pipeline register.
         */
        @Override
        public void advanceClock() {
            // Hint:  You will need to implement this help with waiting
            // for branch resolution and updating the program counter.
            // Don't forget to check for stall conditions, such as when
            // nextStageCanAcceptWork() returns false.
        	 GlobalData globals = (GlobalData)core.getGlobalResources();
        	 if (nextStageCanAcceptWork()) {
             	
        		 output_reg.setMasterBubble(false);
        		
        		 globals.program_counter++;
 			}
        	
        	 
        }
    }

    
    /*** Decode Stage ***/
    static class Decode extends PipelineStageBase<FetchToDecode,DecodeToExecute> {
        public Decode(CpuCore core, PipelineRegister input, PipelineRegister output) {
            super(core, input, output);
        }
        
        @Override
        public boolean stageWaitingOnResource() {
            // Hint:  You will need to implement this to deal with 
            // dependencies.
        	if(input_reg.isSlaveStalled())
        		return true;
        	else
            return false;
        }
        

        @Override
        public void compute(FetchToDecode input, DecodeToExecute output) {
            InstructionBase ins = input.getInstruction();
            
            // You're going to want to do something like this:
            
            // VVVVV LOOK AT THIS VVVVV
            ins = ins.duplicate();
            // ^^^^^ LOOK AT THIS ^^^^^
            
            // The above will allow you to do things like look up register 
            // values for operands in the instruction and set them but avoid 
            // altering the input latch if you're in a stall condition.
            // The point is that every time you enter this method, you want
            // the instruction and other contents of the input latch to be
            // in their original state, unaffected by whatever you did 
            // in this method when there was a stall condition.
            // By cloning the instruction, you can alter it however you
            // want, and if this stage is stalled, the duplicate gets thrown
            // away without affecting the original.  This helps with 
            // idempotency.
            
            //System.out.println("Decode " +ins);
            
            // These null instruction checks are mostly just to speed up
            // the simulation.  The Void types were created so that null
            // checks can be almost completely avoided.
            if (ins.isNull()) return;
            
           
            	
            GlobalData globals = (GlobalData)core.getGlobalResources();
            int[] regfile = globals.register_file;
            
            // Do what the decode stage does:
            // - Look up source operands
            // - Decode instruction
            // - Resolve branches  
            //stage waiting on reesourct if()
            /*if(stageWaitingOnResource()==true) {
            	
            }*/
            input_reg.setSlaveStall(false);
            if(globals.BRA_FLag==true){
            	//globals.BRA_FLag=false;
            	return;
            }
       
            if(input.getInstruction().getSrc2().isRegister()==true) {
    			if(globals.register_invalid[ins.getSrc2().getRegisterNumber()]==true)            	
    				input_reg.setSlaveStall(true);
    		}
    		if(input.getInstruction().getSrc1().isRegister()==true) {
        		if (globals.register_invalid[ins.getSrc1().getRegisterNumber()]==true)             	
            		input_reg.setSlaveStall(true);
            	}
    		if(input.getInstruction().getOper0().isRegister()==true)
    			if(globals.register_invalid[ins.getOper0().getRegisterNumber()]==true) {
        		input_reg.setSlaveStall(true);
    		}
   //invalid
    		//System.out.println(input.getInstruction().getOper0().getRegisterNumber());
            if(input.getInstruction().getOper0().isRegister()==true) {
            	
            	
            	Operand op=ins.getOper0();
            	
            	op.setValue(globals.register_file[ins.getOper0().getRegisterNumber()]);
            	ins.setOper0(op);
            	
            	//output.setInstruction();
            }
           if (input.getInstruction().getSrc1().isRegister()==true) {
            	
            	Operand op=ins.getSrc1();
            	//globals.register_invalid[ins.getSrc1().getRegisterNumber()]=true;
            	op.setValue(globals.register_file[ins.getSrc1().getRegisterNumber()]);
            	ins.setSrc1(op);
			} 

            if (input.getInstruction().getSrc2().isRegister()==true) {
            	Operand op=ins.getSrc2();
            	//globals.register_invalid[ins.getSrc2().getRegisterNumber()]=true;
            	op.setValue(globals.register_file[ins.getSrc2().getRegisterNumber()]);
            	ins.setSrc2(op);
			}
            
           
            if(!input_reg.isSlaveStalled() ){
            	output.setInstruction(ins);	
            	 if(input.getInstruction().getOper0().isRegister()) 
            	globals.register_invalid[ins.getOper0().getRegisterNumber()]=true;
          
            }           
           
            // Set other data that's passed to the next stage.
        }
    }
    

    /*** Execute Stage ***/
    static class Execute extends PipelineStageBase<DecodeToExecute,ExecuteToMemory> {
        public Execute(CpuCore core, PipelineRegister input, PipelineRegister output) {
            super(core, input, output);
        }
       /* @Override
        public boolean stageWaitingOnResource() {
            // Hint:  You will need to implement this to deal with 
            // dependencies.
        	if(input_reg.isSlaveStalled()) {
        		input_reg.setSlaveStall(false);
        		return true;
        	}
        	else
            return false;
        }*/
        @Override
        public void compute(DecodeToExecute input, ExecuteToMemory output) {
            InstructionBase ins = input.getInstruction();
            GlobalData globals = (GlobalData)core.getGlobalResources();
            //System.out.println("execute" +ins);
            if (ins.isNull()) return;
           
            //ins.getSrc1().isRegister();
            int source1 = ins.getSrc1().getValue();
            int source2 = ins.getSrc2().getValue();
            int oper0 =   ins.getOper0().getValue();
            //output.
            //ins.
            int result;
            if (ins.getOpcode().equals(EnumOpcode.BRA)) {
				result=MyALU.executeBRA_INS(ins.getOper0(), ins.getLabelTarget(),ins.getComparison());
				if(result!=-1) {
				globals.program_counter=(result-1);
				//input_reg.setSlaveStall(true);
				 if(input.getInstruction().getOper0().isRegister()==true) {            	
		               	globals.register_invalid[ins.getOper0().getRegisterNumber()]=false;
		            }
				 globals.BRA_FLag=true;
				return;
				}
			}
            else if(ins.getOpcode().equals(EnumOpcode.JMP)){
            		globals.program_counter=ins.getLabelTarget().getAddress()-1;
            		 if(input.getInstruction().getOper0().isRegister()==true) {            	
 		               	globals.register_invalid[ins.getOper0().getRegisterNumber()]=false;
 		            }
 				 globals.BRA_FLag=true;
 				return;
            		}
            else
             result= MyALU.execute(ins.getOpcode(), source1, source2, oper0);
            //ins.setOper0(op);
            output.opertemp=result;          
           
            // Fill output with what passes to Memory stage...
            
            if(globals.BRA_FLag==true){
            	globals.BRA_FLag=false;
            	 if(input.getInstruction().getOper0().isRegister()==true) {            	
		               	globals.register_invalid[ins.getOper0().getRegisterNumber()]=false;
		            }
            	return;
            }
           
            
            	
            output.setInstruction(ins);
            // Set other data that's passed to the next stage.
        }
    }
    

    /*** Memory Stage ***/
    static class Memory extends PipelineStageBase<ExecuteToMemory,MemoryToWriteback> {
        public Memory(CpuCore core, PipelineRegister input, PipelineRegister output) {
            super(core, input, output);
        }

        @Override
        public void compute(ExecuteToMemory input, MemoryToWriteback output) {
            InstructionBase ins = input.getInstruction();
            GlobalData globals= (GlobalData)core.getGlobalResources();
           // System.out.println("memory" +ins);
            if (ins.isNull()) return;
            output.opertemp=input.opertemp;
           if(ins.getOpcode().equals(EnumOpcode.STORE)){
        	   globals.Memory_Block[ins.getSrc1().getValue()]= globals.register_file[ins.getOper0().getRegisterNumber()];
        	   if(input.getInstruction().getOper0().isRegister()==true) {            	
                  	globals.register_invalid[ins.getOper0().getRegisterNumber()]=false;
               }
           }
           
           else
        	   output.setInstruction(ins);
            // Access memory...
                    
            
            // Set other data that's passed to the next stage.
          
        }
    }
    

    /*** Writeback Stage ***/
    static class Writeback extends PipelineStageBase<MemoryToWriteback,VoidLatch> {
        public Writeback(CpuCore core, PipelineRegister input, PipelineRegister output) {
            super(core, input, output);
        }

        @Override
        public void compute(MemoryToWriteback input, VoidLatch output) {
            InstructionBase ins = input.getInstruction();
            
            GlobalData globals = (GlobalData)core.getGlobalResources();
                    
            
            if (ins.isNull()) return;
            
            // Write back result to register file
            int result=input.opertemp;
           
            
            if(input.getInstruction().getOper0().isRegister()==true) 
            globals.register_file[ins.getOper0().getRegisterNumber()]=result;
            if(input.getInstruction().getOper0().isRegister()==true) {            	
               	globals.register_invalid[ins.getOper0().getRegisterNumber()]=false;
            }
            if(ins.getOpcode().equals(EnumOpcode.LOAD)) {
           	   //Operand op=ins.getOper0();
           	   globals.register_file[ins.getOper0().getRegisterNumber()]=globals.Memory_Block[ins.getSrc1().getValue()];
           	   //ins.setOper0(op);
           	   if(input.getInstruction().getOper0().isRegister()==true) {            	
                    	globals.register_invalid[ins.getOper0().getRegisterNumber()]=false;
                 }
              }
           
            if (input.getInstruction().getOpcode() == EnumOpcode.OUT)
            	System.out.println("@@output:"+globals.Memory_Block[ins.getOper0().getValue()]);
            if (input.getInstruction().getOpcode() == EnumOpcode.HALT) {
                // Stop the simulation
            	System.exit(0);
            }
        }
    }
}
