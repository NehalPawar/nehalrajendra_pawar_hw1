/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import utilitytypes.EnumComparison;
import utilitytypes.EnumOpcode;
import utilitytypes.LabelTarget;
import utilitytypes.Operand;

/**
 * The code that implements the ALU has been separates out into a static
 * method in its own class.  However, this is just a design choice, and you
 * are not required to do this.
 * 
 * @author 
 */
public class MyALU {
	static int executeBRA_INS(Operand op,LabelTarget lt, EnumComparison ec) {
		int result=lt.getAddress();//someething
		if(ec.equals(EnumComparison.LT)) {			
			if(op.getValue()<0){
				//true so loop
				
				return lt.getAddress();
			}			
		}
		else if(ec.equals(EnumComparison.EQ)) {
			if(op.getValue()==0) {
				//goto lt
				return lt.getAddress();
			//goahed	
			}			
		}
		else if(ec.equals(EnumComparison.GT)) {
			if(op.getValue()>0) {
				//goto lt
				return lt.getAddress();
			//goahed	
			}	
			
		}	
		else if(ec.equals(EnumComparison.GE)) {
			if(op.getValue()>=0) {
				//goto lt
				return lt.getAddress();
			//goahed	
			}
		}
		 // GlobalData globals= (GlobalData)core.getGlobalResources();
		return -1;
	}
    static int execute(EnumOpcode opcode, int input1, int input2, int oper0) {
        int result = 0;
        
        switch(opcode)
        {
        case ADD:
        	result=input1+input2;
        	oper0=result;
        	break;
        	
        case SUB:
        	result=input1-input2;
        	oper0=result;
        	break;
        case AND://zeor is missing due to interger type
        	result=input1&input2;        	
        	break;
        case OR:
        	result=input1 | input2;        	
        	break;
        case SHL:
        	result=input1 << input2;
        	break;
        case ASR:
        	result=input1 >> input2;
        	break;
        case LSR:
        	result=input1 >>>  input2;
        	break;
        case XOR:
        	result=input1 ^ input2;
        	break;
        case CMP:
        	if(input1 == input2)
        		result=0;
        	else if(input1<input2)
        		result=-1;
        	else
        		result=1;
        	break;
        case ROL:
        	//result=input1 | input2;
        	break;
        case ROR:
        	//result=input1 | input2;
        	break;
        case MULS:
        	result=input1 * input2;
        	break;
        case MULU:
        	//result=input1 | input2;
        	break;
        case DIVS:
        	result=input1 / input2;
        	break;
        case DIVU:
        	//result=input1 / input2;
        	break;
        case MOVC:
        	result=input1;
        	break;
        case LOAD:
        	result=input1+input2;
        	break;
        case STORE:
        	result=input1+input2;
        	break;
        	//BRA, JMP, CALL, 
        	  //  LOAD, STORE, MOVC, OUT,
        	    //HALT, NOP, INVALID, NULL;
        }
        // Implement code here that performs appropriate computations for
        // any instruction that requires an ALU operation.  See
        // EnumOpcode.
        
        return result;
    }    
}
